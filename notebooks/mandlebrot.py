# coding: utf-8
import numpy as np

def mandelbrot(X, Y, max_iterations=1000, verbose=True):
    """Computes the Mandelbrot set.

    Returns a matrix with the escape iteration number of the mandelbrot
    sequence. The matrix contains a cell for every (x, y) couple of the
    X and Y vectors elements given in input. Maximum max_iterations are
    performed for each point
    :param X: set of x coordinates
    :param Y: set of y coordinates
    :param max_iterations: maximum number of iterations to perform before
        forcing to stop the sequence
    :param show_out: flag indicating whether to print on console which line
        number is being computed
    :return: Matrix containing the escape iteration number for every point
        specified in input
    """

    # init the output array
    out_arr = np.zeros((len(Y), len(X)))

    # Iterate of the y coordinates
    for i, y in enumerate(Y):
        for j, x in enumerate(X):
            # n is for checking the number of iterations that take place
            # we limit n upto max_iterations only
            n = 0
            # c is a complex number
            c = x + 1j*y
            # copy value of c to z
            # we need z to keep the absolute value of the complex number within the value 2
            z = c
            # repeat until both conditions are True
            while (n < max_iterations) and (abs(z) <= 2):
            	# replace z with the square of itself added to c
                z = z*z + c
                n += 1
            # store the number of iterations that took place in the output matrix
            out_arr[i, j] = n
    # return the output matrix
    return out_arr

if __name__ == "__main__":
	# Generating some random points uniformly between [0, 1]
	number_of_points = 5
	X = np.random.rand(number_of_points)
	Y = np.random.rand(number_of_points)
	max_iterations = 1000
	print("X coordinates = {}".format(X))
	print("Y coordinates = {}".format(Y))
	# The following matrix shows how many iterations (<= max_iterations) can be done before the set diverges
	# If any value is equal to max_iterations it means it didn't diverge and was terminated due to the condition
	print(mandelbrot(X, Y, max_iterations, True))